import React, { Component } from 'react';
import './style.css';

export default class ModelGlass extends Component {
    render() {
        let { id, price, name, url, desc } = this.props.detail;
        return (
            <div>
                <div className='container d-flex'>
                    <div style={{ width: '18rem' }}>
                        <div className='glassModel'>
                            <img src="./glassesImage/v9.png" alt="" />
                        </div>
                        <img src={this.props.model} className="card-img-top" alt="..." />
                        <div className="card-body">
                            <h5 className="card-title">FENDI F4300</h5>
                            <p className="card-text text-white">Light pink square lenses define these sunglasses, ending with amother of pearl effect tip.</p>
                            <button className="btn btn-warning">WOMEN</button>
                        </div>
                    </div>

                    <img src="./glassesImage/cyber.png" style={{ width: "150px" }} alt="" />
                    <div style={{ width: '18rem' }}>
                        <div className='glassModel'>
                            <img src={url} alt="" />
                        </div>
                        <img src={this.props.model} className="card-img-top" alt="..." />
                        <div className="card-body">
                            <h5 className="card-title">{name}</h5>
                            <p className="card-text text-white">{desc}</p>
                            <button className="btn btn-light">Price: ${price}</button>
                        </div>
                    </div>

                </div>
            </div>
        );
    }
}
// style={{ width: "150px", opacity: "0.8", top: "170px" }}