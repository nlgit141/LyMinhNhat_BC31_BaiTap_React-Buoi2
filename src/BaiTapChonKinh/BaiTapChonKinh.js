import React, { Component } from 'react';
import { DataGlass } from "./DataGlasses";
import Item_Glass from './Item_Glass';
import ModelGlass from './ModelGlass';

export default class BaiTapChonKinh extends Component {
    state = {
        glassArr: DataGlass,
        model: "./glassesImage/model.jpg",
        detail: DataGlass[0]
    };
    renderGlass = () => {
        return this.state.glassArr.map((item) => {
            return <Item_Glass data={item} clickChangeDetail={this.changeDetail} />;
        });
    };
    changeDetail = (glass) => {
        this.setState({
            detail: glass
        });
    };

    render() {
        return (
            <div>
                <ModelGlass model={this.state.model} detail={this.state.detail} />
                <div className='d-flex container' style={{ justifyContent: 'space-between' }}>{this.renderGlass()}</div>
            </div>
        );
    }
}
