import React, { Component } from 'react';

export default class Item_Glass extends Component {

    renderItem = () => {
        let { url } = this.props.data;
        return (<button onClick={() => {
            this.props.clickChangeDetail(this.props.data);
        }} className='btn btn-dark my-2'>
            <img src={url} style={{ width: "50px" }} alt="" />
        </button>
        );
    };
    render() {
        return (
            <div>
                {this.renderItem()}
            </div>
        );
    }
}

