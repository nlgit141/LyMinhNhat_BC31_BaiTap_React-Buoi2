import React, { Component } from 'react';

export default class BaiTapChonXe extends Component {
    state = {
        imgSrc: "./img_state/CarBasic/Products/black-car.jpg"
    };
    changeColor = (color) => {
        this.setState({
            imgSrc: `./img_state/CarBasic/Products/${color}-car.jpg`
        });
    };


    render() {
        return (
            <div className='container py-5'>
                <div className='row'>
                    <div className='col-4'>
                        <img src={this.state.imgSrc} className="w-100" alt="" />
                    </div>
                    <div className='col-8 bg-primary'>
                        <button onClick={() => {
                            this.changeColor("black");
                        }} className='btn btn-dark' > Black</button>
                        <button onClick={() => {
                            this.changeColor("red");
                        }} className='btn btn-danger mx-2'>Red</button>
                        <button onClick={() => {
                            this.changeColor("silver");
                        }} className='btn btn-light mx-2'>sliver</button>
                    </div>
                </div>



            </div >
        );
    }
}
